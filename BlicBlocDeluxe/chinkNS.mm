#import "chinkNS.h"

@implementation chinkNS 

#import <Cocoa/Cocoa.h>
#import <OpenGL/OpenGL.h>
#import <OpenGL/gl.h>
#import <OpenGL/glu.h>

#import "mainController.h"


@implementation chinkNS

- (id)initWithFrame:(NSRect)frameRect
{
    NSOpenGLPixelFormat * pixelFormat = [[NSOpenGLPixelFormat alloc] initWithAttributes:(NSOpenGLPixelFormatAttribute[]) {
        NSOpenGLPFAWindow,
        NSOpenGLPFADoubleBuffer,
        NSOpenGLPFADepthSize, 32,
        nil
    }];
    if(pixelFormat == NULL)
        NSLog(@"pixelFormat == NULL");
    [pixelFormat autorelease];

    self = [super initWithFrame:frameRect pixelFormat:pixelFormat];
    if(self == NULL) {
        NSLog(@"Could not initialise self");
        return NULL;
    }

    [[self openGLContext] makeCurrentContext];
    [self initGL];

    return self;
}

- (void)awakeFromNib
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(windowWillMiniaturize:) name:NSWindowWillMiniaturizeNotification object:[self window]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(windowDidMiniaturize:) name:NSWindowDidMiniaturizeNotification object:[self window]];

    animationTimer = [NSTimer scheduledTimerWithTimeInterval:1/60.0 target:self selector:@selector(timerFired) userInfo:NULL repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:animationTimer forMode:NSEventTrackingRunLoopMode];
    [[NSRunLoop currentRunLoop] addTimer:animationTimer forMode:NSModalPanelRunLoopMode];
    lastTime = [[NSDate date] timeIntervalSince1970];
}

- (void)drawRect:(NSRect)frame
{

    NSLog(@"Chink is up and running. Let's do this thing!");

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(1.0, 1.0, 1.0);
    glOrtho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0); 
    glFlush();

    [self drawGL];
    [[self openGLContext] flushBuffer];



}

#pragma mark OpenGL

- (void)initGL { }

- (void)reshapeGL { }

- (void)drawGL { }

- (void)animate:(float)dt { }

#pragma mark Event Handling

- (void)timerFired
{
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    [self animate:currentTime-lastTime];
    lastTime = currentTime;
}


#pragma mark Loose ends

- (void)setFrame:(NSRect)frame
{
    [super setFrame:frame];
    [self reshapeGL];
}

// To get key events
- (BOOL)acceptsFirstResponder
{
    return YES;
}


@end
