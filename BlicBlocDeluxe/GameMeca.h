/*
 *  GameMeca.h
 *  BlicBlocDeluxe
 *
 *  Created by Rick Sanchez on 9/2/19.
 *  Copyright 2019 __MyCompanyName__. All rights reserved.
 *
 */

#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;

struct position{
	int x;
	int y;
};

class Mecanics{
private:
	// 5 couleurs differentes
	map<int, map<int, int> > matrice;
	vector<unsigned int> nextblocs;
	unsigned int level;
	int score;
	int tailleMatX;
	int tailleMatY;
	int tailleNeBX;

	int getHighCol();//ok
	

public:
	int soundflag;//ok
	Mecanics();//ok
	int progscore;
	bool isover;//ok
	map<int, map<int, int> > getMatrice();//ok
	void setMatrice(map<int, map<int, int> > mtrx);//osef
	
	vector<unsigned int> getNextblocs();//ok
	void setNextblocs(vector<unsigned int> blk);//osef
	
	int getScore();//ok
	string getStrScore();//ok
	void setScore(int scr);//osef
	void incScore(int pts);//osef
	void decScore(int pts);//ok
	
	unsigned int getLevel();//ok
	string getStrLevel();//ok
	void setLevel(unsigned int lvl);//osef
	void incLevel(unsigned int lvl);
	void decLevel(unsigned int lvl);//osef
	
	vector<unsigned int> getTwoNextblocs();//ok
	void GenerateNextBlocs();//ok
	void destroyOneLastBlocs();//ok
	
	void initMatrice(int x, int y);//set to zero ,ok
	void initNextblocs(int x);//set to zero ,ok 
	
	void setPreciseMatrice(int x, int y, int val);//osef
	bool gameIsOver(int x);//ok
	vector< map<int, map<int, int> > > addCubeToMatrice(int x, int val);//ok
	
	vector< position > detectVoid();//ok
	vector< map<int, map<int, int> > > detectAndDestroyMemeAnimate();//ok
	
	void exploreAndvoid();//ok
	vector< position > detectTopBeforeVoid();//ok
	
	int getMoindreGain(int x, int couleur);//ok		
	int getBestMadCol(int couleur);//oik
};
