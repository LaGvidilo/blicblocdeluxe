#import "GameView.h"
#include <OpenGL/gl.h>
#import "mainController.h"
#include <math.h>

int getStepFromLevelDifficultFunction(int x){//x is level
	int z1 = (int) log(x/1000.0);
	int z2 = (int) -abs(z1) * x * sin(x) * 1.00;
	return (int) ( abs( z2 ) );
}

int compositedscore = 0;

vector< map<int, map<int, int> > > animated;
vector< map<int, map<int, int> > > fuckingdestroy;
bool flagovr = false;
bool flagchute = false;
int prochute = 40;
int wid;
int hei;
DataElements::DataElements(map<int, map<int, int> > matriceInit){//init, calculate pos, named, zero color, empty zone 
	matriceRef = matriceInit;
	//initCubes();
}
void DataElements::initCubes(int limitC1,int limitC2,int width,int height){// use matriceRef to reproduce constructor func primary
	//int cr = 108;
	cubes.clear();
	map<int, map<int, int> > matrix = matriceRef;
	for (map<int, map<int, int> >::iterator it=matrix.begin(); it!=matrix.end(); ++it){
		for (map<int, int>::iterator it2=it->second.begin(); it2!=it->second.end(); ++it2){
			element el0;
			el0.MX = it->first;
			el0.MY = it2->first;
			stringstream SS;
			SS << el0.MX << "_" << el0.MY << "_cube";
			el0.name = SS.str();
			el0.couleur = it2->second;
			el0.empty = true;
			// (a faire ici): X,Y Calculate
			/* ( a s'inspirer de �a)
			[bPath moveToPoint:NSMakePoint((it1->first * 55)+limitC1, height-limitC2-(it2->first*55))];
			[bPath lineToPoint:NSMakePoint((it1->first * 55)+limitC1,height-50-limitC2-(it2->first*55))];
			[bPath setLineWidth:50.0];
			*/
			hotbox hb;
			hb.posx1 = ((it->first * 55)+limitC1-23-(23*(it->first)))+(6*it->first);
			hb.posx2 = (((it->first * 55)+limitC1+((46-17)*(it->first+1)))-(36*it->first))+(6*it->first+1);
			hb.posy1 = height-limitC2-(it2->first*55);
			hb.posy2 = height-50-limitC2-(it2->first*55);
			el0.hotb = hb;
			//cout << el0.name << "\tx1: " << el0.hotb.posx1 << "\tx2: "<< el0.hotb.posx2 << "\ty1: "<< el0.hotb.posy1<<"\ty2: "<< el0.hotb.posy2 << endl;
			cubes.push_back(el0);
		}
	}
}	

//hitbox
	//getters & setters
void DataElements::setHitBox(int ID, hotbox hbx){
	if (cubes.size()>ID+1){
		cubes[ID].hotb = hbx;
	}
}
hotbox DataElements::getHitBox(int ID){
	hotbox tmp;
	if (cubes.size()>ID+1){
		tmp = cubes[ID].hotb;
	}
	return tmp;
}
	//verify bool func
element DataElements::GetpointIsHitboxed(int x, int y){
	element el1;
	el1.name = "NONE";
	for (vector<element>::iterator it=cubes.begin(); it!=cubes.end(); ++it){
		int x1 = (*it).hotb.posx1;
		int x2 = (*it).hotb.posx2;
		int y1 = (*it).hotb.posy1;
		int y2 = (*it).hotb.posy2;
		if ( x > x1 ) {
			if ( x < x2 ) {
				if ( y > y2 ) {
					if ( y < y1 ){
						el1 = (*it);
						return el1;
					}
				}
			}
		}
	}
	return el1;
}	
	
int DataElements::pointIsHitboxed(int x, int y){
	int isIt = -1;
	for (vector<element>::iterator it=cubes.begin(); it!=cubes.end(); ++it){
		int x1 = (*it).hotb.posx1;
		int x2 = (*it).hotb.posx2;
		int y1 = (*it).hotb.posy1;
		int y2 = (*it).hotb.posy2;
		if ( x > x1 ) {
			if ( x < x2 ) {
				if ( y > y2 ) {
					if ( y < y1 ){
						isIt = 1;
						return isIt;
					}
				}
			}
		}
	}
	return isIt;
}

//getters & setters elements 
vector<element> DataElements::getCubes(){
	return cubes;
}
element DataElements::getElement(int ID){
	element tmp;
	if (cubes.size()>ID+1){
		tmp = cubes[ID];
	}
	return tmp;
}
void DataElements::setCubes(vector<element> elmts){
	cubes = elmts;
}
void DataElements::appendCube(element elmt){
	cubes.push_back(elmt);
}
void DataElements::changeColorCube(int ID, int clr){
	if (cubes.size()>ID+1){
		cubes[ID].couleur = clr;
	}
}
void DataElements::changeColorCubeFromXYMatrix(int x, int y, int clr){
	int tailleX = matriceRef.size();
	int tailleY = matriceRef[0].size();
	if ( (x < tailleX) and (y < tailleY) ){
		matriceRef[x][y] = clr;
		int posv=0;
		for (int ix=0; ix<x+1; ix++){
			for (int iy=0; iy<y+1; iy++){
				posv++;
			}
		}
		cubes[posv].couleur = clr;
	}
}

// nom facultatif
void DataElements::setName(int ID, string tmp){
	if (cubes.size()>ID+1){
		cubes[ID].name = tmp;
	}
}
// pour l'etat vide ou non
void DataElements::invertEmptyState(int ID){
	if (cubes.size()>ID+1){
		cubes[ID].empty = not(cubes[ID].empty);
	}
}
bool DataElements::getEmptyState(int ID){
	bool tmp = false;
	if (cubes.size()>ID+1){
		tmp = cubes[ID].empty;
	}
	return tmp;
}
void DataElements::setEmptyState(int ID, bool state){
	if (cubes.size()>ID+1){
		cubes[ID].empty = state;
	}
}
void DataElements::toEmpty(int ID){
	if (cubes.size()>ID+1){
		cubes[ID].empty = true;
	}
}
void DataElements::toFull(int ID){
	if (cubes.size()>ID+1){
		cubes[ID].empty = false;
	}
}







@implementation GameView
-(void)ontick{
	//mecas.incLevel(1);
	[self clearGLContext];
	[self setNeedsDisplay:true];
}
-(void)ontickChute{
if (not flagovr){
if (not flagchute){
	//Chute du prochain bloc sur une colonne aleatoire
	//effects placed
	int posbadx = mecas.getBestMadCol(blocbuffer[0]);
	animated.clear();
	animated = mecas.addCubeToMatrice(posbadx, blocbuffer[0]);
	mecas.destroyOneLastBlocs();
	blocbuffer = mecas.getTwoNextblocs();
	mecas.GenerateNextBlocs();
	
	//son de chute pas gentille
	NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"hit" ofType:@"wav"];
	NSSound *sound = [[NSSound alloc] initWithContentsOfFile:resourcePath byReference:YES];
	// Do something with sound, like [sound play] and release.
	[sound play];
} else {
	//cout << prochute << " - " << flagchute << endl; 
	if (prochute>0){
		prochute--;
		cout << "Secondes restantes avant algorithme de mechancetee: " << prochute << endl;
		int coefstress = 1;
		if (prochute < 11){
			coefstress = 4;
		}else if (prochute < 21){
			coefstress = 3;
		}else if (prochute < 31){
			coefstress = 2;
		}
		for (int g=0; g<coefstress; g++){
			NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"footstep" ofType:@"wav"];
			NSSound *sound = [[NSSound alloc] initWithContentsOfFile:resourcePath byReference:YES];
			// Do something with sound, like [sound play] and release.
			[sound play];
		}
	}
	if (prochute==0){
		flagchute = false;
	}
}
}
}

- (void)keyDown:(NSEvent *)theEvent{

}
// ---------------------------------

- (void)rightMouseDown:(NSEvent *)theEvent // pan
{
	NSPoint location = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	cout << "Right Mouse Clic ("<<location.x<<" , "<<location.y<<")"<<endl;
}

// ---------------------------------

- (void)otherMouseDown:(NSEvent *)theEvent //dolly
{
	NSPoint location = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	cout << "Other Mouse Clic ("<<location.x<<" , "<<location.y<<")"<<endl;
}

- (void)normalMouseDown:(NSEvent *)theEvent //dolly
{
	NSPoint location = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	cout << "Normal Mouse Clic ("<<location.x<<" , "<<location.y<<")"<<endl;
	if ( 1 == DElem.pointIsHitboxed(location.x,location.y) ){
		cout << "Element hitboxed by mouse: " << endl;
		element el0 = DElem.GetpointIsHitboxed(location.x, location.y);
		cout << "|"<<"name"<<"--->"<<el0.name<<endl;
		cout << "|"<<"empty"<<"--->"<<el0.empty<<endl;
		cout << "|"<<"color"<<"--->"<<el0.couleur<<endl;
		cout << "|"<<"Matrix X POS"<<"--->"<<el0.MX<<endl;
		cout << "|"<<"Matrix Y POS"<<"--->"<<el0.MY<<endl;
		cout << endl << endl;
		
		//effects placed
		animated.clear();
		animated = mecas.addCubeToMatrice(el0.MX, blocbuffer[0]);
		compositedscore = mecas.progscore;
		int z3 = sin(mecas.getLevel()*mecas.getLevel())+log(mecas.getLevel())*1.00+40;
		cout<< z3 << " <---=max_prochute = -- "<< getStepFromLevelDifficultFunction((int)mecas.getLevel()) <<" -- = compositedscore=---> "<<compositedscore <<endl;
		prochute = min(prochute+rand()%2,abs(z3)); 
		
		if ( compositedscore > getStepFromLevelDifficultFunction((int)mecas.getLevel()) ){
			mecas.incLevel(1);
			mecas.progscore = 0;
			compositedscore = 0;
		}
		mecas.destroyOneLastBlocs();
		blocbuffer = mecas.getTwoNextblocs();
		mecas.GenerateNextBlocs();
		//fucking destuction effects
		fuckingdestroy.clear();
		fuckingdestroy = mecas.detectAndDestroyMemeAnimate();
		
		//loss score points
		mecas.decScore(mecas.getLevel()*5);
		
		if (not mecas.isover){
			flagchute = true;
			//Sound click
			NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"click" ofType:@"wav"];
			NSSound *sound = [[NSSound alloc] initWithContentsOfFile:resourcePath byReference:YES];
			// Do something with sound, like [sound play] and release.
			[sound play];
		}
	}
	
}


- (void) mouseUp:(NSEvent *)theEvent{

}
- (void) rightMouseUp:(NSEvent *)theEvent{

}
- (void) otherMouseUp:(NSEvent *)theEvent{

}
- (void) mouseDragged:(NSEvent *)theEvent{

}
- (void) scrollWheel:(NSEvent *)theEvent{

}
- (void) rightMouseDragged:(NSEvent *)theEvent{

}
- (void) otherMouseDragged:(NSEvent *)theEvent{

}
- (void) awakeFromNib{
	if (flag == true){
		blocbuffer = mecas.getTwoNextblocs();
	}
}


- (void)mouseDown:(NSEvent *)theEvent{
    if ([theEvent modifierFlags] & NSControlKeyMask){ // send to pan
		[self rightMouseDown:theEvent];
		//cout << "Right Mouse Clic ("<<location.x<<" , "<<location.y<<")"<<endl;
	}
	else if ([theEvent modifierFlags] & NSAlternateKeyMask){ // send to dolly
		[self otherMouseDown:theEvent];
		//cout << "Other Mouse Clic ("<<location.x<<" , "<<location.y<<")"<<endl;
	}
	else {
		[self normalMouseDown:theEvent];
		//location = [self convertPoint:[theEvent locationInWindow] fromView:nil];
		//location.x, location.y;
		//cout << "Mouse Clic ("<<location.x<<" , "<<location.y<<")"<<endl;
	}
}

-(void)drawRect:(NSRect)dirtyRect{
	int width = dirtyRect.size.width;
	int height = dirtyRect.size.height;
	int limitC1 = width/3;
	int limitC2 = 215;
	int cr = 108;
	if (not mecas.isover){
	if (flag == true){
		timerChute=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(ontickChute) userInfo:nil repeats:true];
		t=[NSTimer scheduledTimerWithTimeInterval:0.001 target:self selector:@selector(ontick) userInfo:nil repeats:true];
		flag = false;
		textFieldlvl = [[NSTextField alloc] initWithFrame:NSMakeRect(215,250,75,30)];
		textFieldpts = [[NSTextField alloc] initWithFrame:NSMakeRect(limitC1-(60/2),610,60*6-cr+30,40)]; 
	}
	else{
		DElem.initCubes(limitC1, limitC2, width, height);
	}
	[super drawRect:dirtyRect];
	
		
		//Sounds
		if (mecas.soundflag==1){
			NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"cast" ofType:@"wav"];
			NSSound *sound = [[NSSound alloc] initWithContentsOfFile:resourcePath byReference:YES];
			// Do something with sound, like [sound play] and release.
			[sound play];
			mecas.soundflag = 0;
		}
	
		//fucking destuction effects
		fuckingdestroy.clear();
		fuckingdestroy = mecas.detectAndDestroyMemeAnimate();
		
	//background set
	[[NSColor colorWithCalibratedRed:0.10 green:0.10 blue:0.05 alpha:1.0] set];
	[[NSBezierPath bezierPathWithRect:[self bounds]] fill];

	//ligne bas
	[[NSColor yellowColor] set];
	NSBezierPath *bPath = [NSBezierPath bezierPath];
	[bPath moveToPoint:NSMakePoint(limitC1-(60/2), limitC2-20)];
	[bPath lineToPoint:NSMakePoint(limitC1+60*6-cr, limitC2-20)];
	[bPath moveToPoint:NSMakePoint(limitC1-(60/2), limitC2-20-2)];
	[bPath lineToPoint:NSMakePoint(limitC1-(60/2), height-limitC2+20+2)];
	[bPath moveToPoint:NSMakePoint(limitC1+60*6-cr, limitC2-20-2)];
	[bPath lineToPoint:NSMakePoint(limitC1+60*6-cr, height-limitC2+20+2)];
	[bPath setLineWidth:4.0];
	[bPath stroke];

	// UI
	[[NSColor yellowColor] set];
	bPath = [NSBezierPath bezierPath];
	[bPath moveToPoint:NSMakePoint(limitC1-(60/2), height-limitC2+20)];
	[bPath lineToPoint:NSMakePoint(limitC1+60*6+64-cr,height-limitC2+20)];
	[bPath lineToPoint:NSMakePoint(limitC1+60*6+64-cr,height-limitC2+20-65*2)];
	[bPath lineToPoint:NSMakePoint(limitC1+60*6-cr,height-limitC2+20-65*2)];
	[bPath setLineWidth:4.0];
	[bPath stroke];
	
	// UI
	[[NSColor yellowColor] set];
	bPath = [NSBezierPath bezierPath];
	[bPath moveToPoint:NSMakePoint(limitC1-(60/2), limitC2-20)];
	[bPath lineToPoint:NSMakePoint(limitC1-120, limitC2-20)];
	[bPath lineToPoint:NSMakePoint(limitC1-120, limitC2-20+90)]; //limitC1-120, limitC2-20+90
	[bPath lineToPoint:NSMakePoint(limitC1-(60/2), limitC2-20+90)];
	[bPath setLineWidth:4.0];
	[bPath stroke];

	NSString *scoremsg = [NSString stringWithCString: mecas.getStrScore().c_str() encoding: NSUTF8StringEncoding];
	NSString *levelmsg = [NSString stringWithCString: mecas.getStrLevel().c_str() encoding: NSUTF8StringEncoding];

	map<int, map<int, int> > matrix;// = mecas.getMatrice();
	if (animated.size()>0){
		map<int, map<int, int> > matrix = animated[0];
		animated.erase(animated.begin());
	}
	else if (fuckingdestroy.size()>0){
		map<int, map<int, int> > matrix = fuckingdestroy[0];
		fuckingdestroy.erase(fuckingdestroy.begin());
	}
	else{
		matrix = mecas.getMatrice();
	}
	
	for (map<int, map<int, int> >::iterator it1=matrix.begin(); it1!=matrix.end(); ++it1){
		for (map<int, int>::iterator it2=it1->second.begin(); it2!=it1->second.end(); ++it2){
			if (it2->second == 0){
				[[NSColor blackColor] set];
			}
			else if (it2->second == 1){
				[[NSColor blueColor] set];
			}
			else if (it2->second == 2){
				[[NSColor redColor] set];
			}
			else if (it2->second == 3){
				[[NSColor greenColor] set];
			}
			else if (it2->second == 4){
				[[NSColor yellowColor] set];
			}
			else if (it2->second == 5){
				[[NSColor orangeColor] set];
			}
			bPath = [NSBezierPath bezierPath];
			[bPath moveToPoint:NSMakePoint((it1->first * 55)+limitC1, height-limitC2-(it2->first*55))];
			[bPath lineToPoint:NSMakePoint((it1->first * 55)+limitC1,height-50-limitC2-(it2->first*55))];
			[bPath setLineWidth:50.0];
			[bPath stroke];
		}
	}
    
	int j = 0;
	for (vector<unsigned int>::iterator it=blocbuffer.begin(); it!=blocbuffer.end(); ++it){
		if ((*it) == 0){
			[[NSColor blackColor] set];
		}
		else if ((*it) == 1){
			[[NSColor blueColor] set];
		}
		else if ((*it) == 2){
			[[NSColor redColor] set];
		}
		else if ((*it) == 3){
			[[NSColor greenColor] set];
		}
		else if ((*it) == 4){
			[[NSColor yellowColor] set];
		}
		else if ((*it) == 5){
			[[NSColor orangeColor] set];
		}
		bPath = [NSBezierPath bezierPath];
		[bPath moveToPoint:NSMakePoint((0 * 50)+580+15, 590-5-(j * 50))];
		[bPath lineToPoint:NSMakePoint((0 * 50)+580+15, 590-45-(j * 50))];
		[bPath setLineWidth:40.0];
		[bPath stroke];
		j++;
	}
	

    
    [textFieldlvl setStringValue:levelmsg];
	[textFieldlvl setAlignment: NSCenterTextAlignment];
	[textFieldlvl sizeToFit];
    [textFieldlvl setBezeled:NO];
    [textFieldlvl setDrawsBackground:NO];
    [textFieldlvl setEditable:NO];
    [textFieldlvl setSelectable:NO];
	[textFieldlvl setTextColor: [NSColor yellowColor]];    
	[self addSubview:textFieldlvl];


    [textFieldpts setStringValue:scoremsg];
	[textFieldpts setAlignment: NSCenterTextAlignment];
	//[textFieldpts sizeToFit];
    [textFieldpts setBezeled:NO];
    [textFieldpts setDrawsBackground:NO];
    [textFieldpts setEditable:NO];
    [textFieldpts setSelectable:NO];
	[textFieldpts setTextColor: [NSColor yellowColor]];    
	[self addSubview:textFieldpts];
	}
	if (mecas.isover and not(flagovr)){
		flagchute = true;
		NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"death1" ofType:@"wav"];
		NSSound *sound = [[NSSound alloc] initWithContentsOfFile:resourcePath byReference:YES];
		// Do something with sound, like [sound play] and release.
		[sound play];
		
		flagovr = true;
		/*if ([timerChute isValid]) {
			[timerChute invalidate];
		}
		timerChute = nil;*/
		string messagedefin = mecas.getStrScore()+" Points.\nMeme un sims ne fait pas mieux alors ne t'inquiete pas!";
		NSString *msgfin = [NSString stringWithCString: messagedefin.c_str() encoding: NSUTF8StringEncoding];
		NSAlert *alert = [[NSAlert alloc] init];
		[alert setMessageText:@"Tu est mourrut !"];
		[alert setInformativeText:msgfin];
		[alert addButtonWithTitle:@"Fin de la partie"];
		[alert runModal];
	}

}
- (IBAction)newgame:(id)sender
{
	NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"unlock" ofType:@"wav"];
	NSSound *sound = [[NSSound alloc] initWithContentsOfFile:resourcePath byReference:YES];
	// Do something with sound, like [sound play] and release.
	[sound play];
	mecas = Mecanics();
	mecas.GenerateNextBlocs();
	blocbuffer = mecas.getTwoNextblocs();
	flagovr = false;
	flagchute = false;
	prochute = 40;
}

- (IBAction)pausegame:(id)sender
{
	NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"equip" ofType:@"wav"];
	NSSound *sound = [[NSSound alloc] initWithContentsOfFile:resourcePath byReference:YES];
	// Do something with sound, like [sound play] and release.
	[sound play];
	cout << "pause a programmer ici" << endl;
	//tuer le timer de chute
	flagchute = true;
	prochute = -1;
}
@end
