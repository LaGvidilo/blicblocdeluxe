//
//  main.m
//  BlicBlocDeluxe
//
//  Created by Rick Sanchez on 9/2/19.
//  Copyright __MyCompanyName__ 2019. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc,  (const char **) argv);
}
