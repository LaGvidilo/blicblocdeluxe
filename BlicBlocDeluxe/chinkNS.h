/* chinkNS */

#import <Cocoa/Cocoa.h>

@interface chinkNS : NSOpenGLView
{

- (void)initGL;
- (void)reshapeGL;
- (void)drawGL;
- (void)animate:(float)dt;

}
@end
