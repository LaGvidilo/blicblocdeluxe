/*
 *  GameMeca.cpp
 *  BlicBlocDeluxe
 *
 *  Created by Rick Sanchez on 9/2/19.
 *  Copyright 2019 __MyCompanyName__. All rights reserved.
 *
 */

#include "GameMeca.h"


/* Mecanics */
Mecanics::Mecanics(){
	srand(time(0));
	matrice.clear();
	nextblocs.clear();
	level = 1;
	score = 100;
	initMatrice(5,7);
	initNextblocs(1024);
	GenerateNextBlocs();
	isover = false;
	soundflag = 0;
	progscore = 0;
}

map<int, map<int, int> > Mecanics::getMatrice(){
	return matrice;
}
void Mecanics::setMatrice(map<int, map<int, int> > mtrx){
	matrice = mtrx;
}
	
vector<unsigned int> Mecanics::getNextblocs(){
	return nextblocs;
}
void Mecanics::setNextblocs(vector<unsigned int> blk){
	blk = nextblocs;
}
	
int Mecanics::getScore(){
	return score;
}
string Mecanics::getStrScore(){
	stringstream SS;
	SS << "Score\n" <<score;
	return SS.str();
}
void Mecanics::setScore(int scr){
	score = scr;
}
void Mecanics::incScore(int pts){
	score += pts;
}
void Mecanics::decScore(int pts){
	score -= pts;
	if (score<0){score=0;}
}
	
unsigned int Mecanics::getLevel(){
	return level;
}
string Mecanics::getStrLevel(){
	stringstream SS;
	SS << "LEVEL\n" <<level;
	return SS.str();
}
void Mecanics::setLevel(unsigned int lvl){
	level = lvl;
}
void Mecanics::incLevel(unsigned int lvl){
	level += 1;
}
void Mecanics::decLevel(unsigned int lvl){
	level -= 1;
}
	
vector<unsigned int> Mecanics::getTwoNextblocs(){
	vector<unsigned int> reponses;
	reponses.push_back(nextblocs[0]);
	reponses.push_back(nextblocs[1]);
	return reponses;
}
void Mecanics::GenerateNextBlocs(){
	for (int i=0; i<tailleNeBX; i++){
		if (nextblocs.size()<tailleNeBX){
			nextblocs.push_back( 1+ (rand()%5) );
		}
		else {
			if (nextblocs[i]==0){
				nextblocs[i] = 1+ (rand()%5);
			}
		}
	}
}
void Mecanics::destroyOneLastBlocs(){
	nextblocs.erase(nextblocs.begin());
}
	
void Mecanics::initMatrice(int x, int y){//set to zero
	tailleMatX = x;
	tailleMatY = y;
	for (int ix=0; ix<tailleMatX; ix++){
		for (int iy=0; iy<tailleMatY; iy++){
			matrice[ix][iy] = 0;
		}
	}
}
void Mecanics::initNextblocs(int x){//set to zero
	tailleNeBX = x;
	nextblocs.clear();
	for (int i=0; i<x; i++){
		nextblocs.push_back(0);
	}
}

void Mecanics::setPreciseMatrice(int x, int y, int val){
	matrice[x][y] = val;
}

bool Mecanics::gameIsOver(int x){
	return (matrice[x][0] != 0);
}

vector< map<int, map<int, int> > > Mecanics::addCubeToMatrice(int x, int val){
	vector< map<int, map<int, int> > > listmat;
	int tmpy=0;
	if (not gameIsOver(x)){
		map<int, map<int, int> > tmp;
		for (int y=0; y<tailleMatY; y++){
			tmp.clear();
			tmp = matrice;
			if (tmp[x][y] == 0){
				tmpy=y;
				tmp[x][y] = val;
				listmat.push_back(tmp);
			}
		}
		matrice[x][tmpy] = val;
	}
	else{
		isover = true;
	}
	return listmat;//if size() == 0 alors gameover
}

vector< position > Mecanics::detectVoid(){
	vector< position > vectpos;
	
	for (map<int, map<int, int> >::iterator it=matrice.begin(); it!=matrice.end(); ++it){//x
		for (map<int, int>::iterator it2=it->second.begin(); it2!=it->second.end(); ++it2){
			position posi;
			posi.x = it->first;
			posi.y = it2->first;
			if (it2->second == 0){
				vectpos.push_back(posi);
			}
		}
	}
	
	return vectpos;
}

vector< position > Mecanics::detectTopBeforeVoid(){
	vector< position > vectvoidpos = detectVoid();
	vector< position > vectpos;
	
	for (vector< position >::iterator it=vectvoidpos.begin(); it!=vectvoidpos.end(); ++it){
		if ((*it).y>0){
			for (int y=(*it).y-1; y>0; y--){
				if (matrice[(*it).x][y] != 0){
					//if (matrice[(*it).x][y+1] == 0){
					position posi;
					posi.x = (*it).x;
					posi.y = y+1;
					//vectpos.push_back(posi);
					//}
				}
			}
		}
	}

	return vectpos;
}


vector< map<int, map<int, int> > > Mecanics::detectAndDestroyMemeAnimate(){
	vector< map<int, map<int, int> > > frames;
	map<int, map<int, int> > mattmp = matrice;
	exploreAndvoid();
	long taille_posadel = 1;
	while(taille_posadel>0){
		vector< position > posadel = detectTopBeforeVoid();
		taille_posadel = posadel.size();
	
		for (vector< position >::iterator it=posadel.begin(); it!=posadel.end(); ++it){
			if ( (*it).y < tailleMatY ) {
				mattmp[(*it).x][(*it).y+1] = mattmp[(*it).x][(*it).y];
				mattmp[(*it).x][(*it).y] = 0;
				frames.push_back(mattmp);
			}
		}
	}
	return frames;
}


void Mecanics::exploreAndvoid(){
	soundflag = 0;
	/* Explore and prepare void by flaged */
	vector< position > posoftruc;
	for (int x=0; x<tailleMatX; x++){
		for (int y=0; y<tailleMatY; y++){
			int couleur = abs(matrice[x][y]);
			if (couleur>0){
				int count = 0;
				// direction 1
				for (int l=x+1; l<tailleMatX; l++){
					if (abs(matrice[l][y]) == couleur){
						count += 1;
						position posi;
						posi.x = l;
						posi.y = y;
						posoftruc.push_back(posi);
					}
					else {
						break;
					}
				}
			
				// direction 2
				for (int l=x-1; l>0; l--){
					if (abs(matrice[l][y]) == couleur){
						count += 1;
						position posi;
						posi.x = l;
						posi.y = y;
						posoftruc.push_back(posi);
					}
					else {
						break;
					}
				}
			
				// direction 3
				for (int h=y+1; h<tailleMatY; h++){
					if (abs(matrice[x][h]) == couleur){
						count += 1;
						position posi;
						posi.x = x;
						posi.y = h;
						posoftruc.push_back(posi);
					}
					else {
						break;
					}
				}
			
				// direction 4
				for (int h=y-1; h>0; h--){
					if (abs(matrice[x][h]) == couleur){
						count += 1;
						position posi;
						posi.x = x;
						posi.y = h;
						posoftruc.push_back(posi);
					}
					else {
						break;
					}
				}
				
				if ( count>1 ){
					if ( (x+1 < tailleMatX) and (y+1 < tailleMatY) ){
						if (abs(matrice[x+1][y+1]) == couleur){
							count += 1;
							position posi;
							posi.x = x+1;
							posi.y = y+1;
							posoftruc.push_back(posi);
						}
					}
					if ( (x-1 >= 0) and (y-1 >= 0) ){
						if (abs(matrice[x-1][y-1]) == couleur){
							count += 1;
							position posi;
							posi.x = x-1;
							posi.y = y-1;
							posoftruc.push_back(posi);
						}
					}
					if ( (x+1 < tailleMatX) and (y-1 >= 0) ){
						if (abs(matrice[x+1][y-1]) == couleur){
							count += 1;
							position posi;
							posi.x = x+1;
							posi.y = y-1;
							posoftruc.push_back(posi);
						}
					}
					if ( (x-1 >= 0) and (y+1 < tailleMatY) ){
						if (abs(matrice[x-1][y+1]) == couleur){
							count += 1;
							position posi;
							posi.x = x-1;
							posi.y = y+1;
							posoftruc.push_back(posi);
						}
					}
				}
			
				if (count >= 3){
					if (matrice[x][y] > 0){ matrice[x][y] = - ( matrice[x][y] ); }
					for (vector< position >::iterator it=posoftruc.begin(); it!=posoftruc.end(); ++it){
						if (matrice[(*it).x][(*it).y] > 0){
							progscore = score;
							score += 300 + ((count-3)*100);
							matrice[(*it).x][(*it).y] = - ( matrice[(*it).x][(*it).y] );
						}
					}
					posoftruc.clear();
					count = 0;
					//break;
				}else{
					posoftruc.clear();
					count = 0;
				}
			
			}
		}
	}
	
	
	/* Void the flaged */
	for (int x=0; x<tailleMatX; x++){
		for (int y=0; y<tailleMatY; y++){
			int couleur = matrice[x][y];
			if (couleur < 0){
				matrice[x][y] = 0;
				soundflag = 1;
			}
		}
	}
	
	/* Gravity One Shot */
	for (int x=0; x<tailleMatX; x++){
		for (int y=0; y<tailleMatY; y++){
			int couleur = matrice[x][y];
			if (couleur > 0){
				for (int h=y; h<tailleMatY; h++){
					int bottomcolor = matrice[x][h];
					if (bottomcolor == 0){
						matrice[x][h] = matrice[x][y];
						matrice[x][y] = 0;
					}
				}
			}
		}
	}
}

int Mecanics::getHighCol(){
	int x=0;
	int y=tailleMatY;
	for (map<int, map<int, int> >::iterator it=matrice.begin(); it!=matrice.end(); ++it){
		for (map<int, int>::iterator ity=it->second.begin(); ity!=it->second.end(); ++ity){
			if ( ity->second > 0 ){
				if ( ity->first < y ){
					y = ity->second;
					x = it->first;
				}
			}
		}
	}
	return x;
}

int Mecanics::getMoindreGain(int x, int couleur){
	soundflag = 0;
	int count = 0;
	/* Explore and prepare void by flaged */
	vector< position > posoftruc;
		for (int y=0; y<tailleMatY; y++){
			if (couleur>0){
				int count = 0;
				// direction 1
				for (int l=x+1; l<tailleMatX; l++){
					if (abs(matrice[l][y]) == couleur){
						count += 1;
						position posi;
						posi.x = l;
						posi.y = y;
						posoftruc.push_back(posi);
					}
					else {
						break;
					}
				}
			
				// direction 2
				for (int l=x-1; l>0; l--){
					if (abs(matrice[l][y]) == couleur){
						count += 1;
						position posi;
						posi.x = l;
						posi.y = y;
						posoftruc.push_back(posi);
					}
					else {
						break;
					}
				}
			
				// direction 3
				for (int h=y+1; h<tailleMatY; h++){
					if (abs(matrice[x][h]) == couleur){
						count += 1;
						position posi;
						posi.x = x;
						posi.y = h;
						posoftruc.push_back(posi);
					}
					else {
						break;
					}
				}
			
				// direction 4
				for (int h=y-1; h>0; h--){
					if (abs(matrice[x][h]) == couleur){
						count += 1;
						position posi;
						posi.x = x;
						posi.y = h;
						posoftruc.push_back(posi);
					}
					else {
						break;
					}
				}
				
				if ( count>1 ){
					if ( (x+1 < tailleMatX) and (y+1 < tailleMatY) ){
						if (abs(matrice[x+1][y+1]) == couleur){
							count += 1;
							position posi;
							posi.x = x+1;
							posi.y = y+1;
							posoftruc.push_back(posi);
						}
					}
					if ( (x-1 >= 0) and (y-1 >= 0) ){
						if (abs(matrice[x-1][y-1]) == couleur){
							count += 1;
							position posi;
							posi.x = x-1;
							posi.y = y-1;
							posoftruc.push_back(posi);
						}
					}
					if ( (x+1 < tailleMatX) and (y-1 >= 0) ){
						if (abs(matrice[x+1][y-1]) == couleur){
							count += 1;
							position posi;
							posi.x = x+1;
							posi.y = y-1;
							posoftruc.push_back(posi);
						}
					}
					if ( (x-1 >= 0) and (y+1 < tailleMatY) ){
						if (abs(matrice[x-1][y+1]) == couleur){
							count += 1;
							position posi;
							posi.x = x-1;
							posi.y = y+1;
							posoftruc.push_back(posi);
						}
					}
				}
			
			}
		}
	return count;
}

int Mecanics::getBestMadCol(int couleur){
	int moindrepos = 0;
	int gain;
	int mingain = 999*1000;
	for (int x=0; x<tailleMatX; x++){
		gain = getMoindreGain(x, couleur);
		if (gain < mingain){
			moindrepos = x;
			mingain = gain;
		}
	}
	for (int x=0; x<tailleMatX; x++){
		gain = getMoindreGain(x, couleur);
		if (gain == mingain){
			if ( mingain >= getMoindreGain(getHighCol(), couleur) ) {
				return getHighCol();
			}
		}
	}
	return moindrepos;
}