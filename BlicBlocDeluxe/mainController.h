/* mainController */

#import <Cocoa/Cocoa.h>
/*
#import <OpenGL/OpenGL.h>
#import <OpenGL/gl.h>
#import <OpenGL/glu.h>
#include "GameMeca.h"
*/
#include <string>
#include <vector>
#include <map>
#include <iostream>

using namespace std;

@interface mainController : NSWindowController
{
    //IBOutlet NSOpenGLView *graphic;

    IBOutlet NSTextField *lvl;
    IBOutlet NSTextField *pts;
	
} 
- (void)updateTextField: (NSString *)updatedString;
+(void)aMethod:(id)param;
- (IBAction)setpts:(NSString*)msg;
- (void)setlvl:(NSString*)msg;
- (IBAction)newgame:(id)sender;
- (IBAction)pausegame:(id)sender;
@end
