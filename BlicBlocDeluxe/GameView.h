/* GameView */
#import <OpenGL/gl.h>
#import <OpenGL/glext.h>
#import <OpenGL/glu.h>
#import <Cocoa/Cocoa.h>
#include <string>
#include <map>
#include <vector>
#include <sstream>
#include "GameMeca.h"

//drapeau du demarrage
bool flag = true;

//pour la souris
NSPoint location;
//toujours pour la souris mais specifique pour les elements a clicker
struct hotbox{
	int posx1;
	int posy1;
	int posx2;
	int posy2;
};
struct element{
	hotbox hotb;
	string name;
	bool empty;
	;int couleur;
	int MX;
	int MY;
};

class DataElements{
private:
	vector<element> cubes;
	map<int, map<int, int> > matriceRef;

public:
	DataElements(map<int, map<int, int> > matriceInit);//init, calculate pos, named, zero color, empty zone 
	
	//getters & setters elements 
	vector<element> getCubes();
	element getElement(int ID);
	void setCubes(vector<element> elmts);
	void appendCube(element elmt);
	void initCubes(int limitC1,int limitC2,int width,int height);// use matriceRef to reproduce constructor func primary
	void changeColorCube(int ID, int clr);
	void changeColorCubeFromXYMatrix(int x, int y, int clr);
	
	// nom facultatif
	void setName(int ID, string tmp);
	
	// pour l'etat vide ou non
	void invertEmptyState(int ID);
	bool getEmptyState(int ID);
	void setEmptyState(int ID, bool state);
	void toEmpty(int ID);
	void toFull(int ID);
	
	//hitbox
		//getters & setters
	void setHitBox(int ID, hotbox hbx);
	hotbox getHitBox(int ID);
		//verify bool func
	int pointIsHitboxed(int x, int y);
	element GetpointIsHitboxed(int x, int y);
	
};

//coeur du jeu
Mecanics mecas;
DataElements DElem(mecas.getMatrice());
vector<unsigned int> blocbuffer;

@interface GameView : NSOpenGLView
{
	NSTimer *t;
	NSTimer *timerChute;
	NSTextField *textFieldlvl;
	NSTextField *textFieldpts;
}
- (void)keyDown:(NSEvent *)theEvent;

- (void) mouseDown:(NSEvent *)theEvent;
- (void) rightMouseDown:(NSEvent *)theEvent;
- (void) otherMouseDown:(NSEvent *)theEvent;
- (void) mouseUp:(NSEvent *)theEvent;
- (void) rightMouseUp:(NSEvent *)theEvent;
- (void) otherMouseUp:(NSEvent *)theEvent;
- (void) mouseDragged:(NSEvent *)theEvent;
- (void) scrollWheel:(NSEvent *)theEvent;
- (void) rightMouseDragged:(NSEvent *)theEvent;
- (void) otherMouseDragged:(NSEvent *)theEvent;
- (void) awakeFromNib;
- (IBAction)newgame:(id)sender;
- (IBAction)pausegame:(id)sender;
@end
